<?php

if (!function_exists('custom_route')) {

    /**
     * Generate the URL to a named route.
     *
     * @param  array|string  $name
     * @param  mixed  $parameters
     * @param  bool  $absolute
     * @return string
     */
    function custom_route($name, $parameters = [], $absolute = true) {
        if (request()->getHost() !== env('APP_DOMAIN')) {
            return app('url')->route($name, array_merge($parameters, ['companyIdentifier' => request('companyIdentifier')]), $absolute);
        } else {
            return app('url')->route($name, $parameters, $absolute);
        }
    }

}