<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
//        dd(1230);
        session()->flash('status', 'Hello Tenant' . ((request('companyIdentifier') !== null)  ?  ' '.request('companyIdentifier')   :   '') );
        return view('home');
    }

}
