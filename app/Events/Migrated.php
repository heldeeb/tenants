<?php

/*
 * This file is part of the hyn/multi-tenant package.
 *
 * (c) Daniël Klabbers <daniel@klabbers.email>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://laravel-tenancy.com
 * @see https://github.com/hyn/multi-tenant
 */

namespace App\Events;

use Hyn\Tenancy\Abstracts\WebsiteEvent;

class Migrated extends WebsiteEvent {

    /**
     * @var Website
     */
    public $website;

    /**
     * @var Hostname|null
     */
    public $hostname;

    public function __construct(Website &$website, Hostname $hostname = null) {
        $this->website = &$website;
        $this->hostname = $hostname;

        dd($this->website);

    }

}
