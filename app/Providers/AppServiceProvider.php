<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //NEW: Import Schema

class AppServiceProvider extends ServiceProvider {

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    function boot() {
        Schema::defaultStringLength(191); //NEW: Increase StringLength

        if (!app()->runningInConsole() && request()->getHost()) {
            if (request()->getHost() == str_replace('http://', '', env('APP_URL'))) {
                config(['auth.defaults.guard' => 'sys']);
            } else {
                config(['database.default' => 'tenant']);
            }
        }
    }

}
