<?php

namespace App\Models;

//use App\Models\System\Plan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Hyn\Tenancy\Models\Website;
use Hyn\Tenancy\Models\Hostname;
use Hyn\Tenancy\Repositories\WebsiteRepository;
use Hyn\Tenancy\Repositories\HostnameRepository;
use Illuminate\Support\Facades\Artisan;

/**
 * @property Website website
 * @property Hostname hostname
 */
class Tenancy {

    public $website;
    public $hostname;

    public function __construct(Website $website = null, Hostname $hostname = null) {
//        dd(456);
        $this->website = $website;
        $this->hostname = $hostname;
    }

    public static function delete($hostname, $website) {
        app(WebsiteRepository::class)->delete($website, true);
        app(HostnameRepository::class)->delete($hostname, true);
        return true;
    }

//    public static function create($data): Tenancy {
    public static function create($data) {
        // Create New Website
        $website = new Website;
        $website->uuid = "hyn_" . $data['name'] . '_' . Str::random(5);

        app(WebsiteRepository::class)->create($website);

        if ($data['name'] == 'custom') {
            //    "/var/www/html/hyn/database/migrations/tenant/custom"
            //    dd(database_path('migrations/tenant') . '/custom');

            Artisan::call(
                    'tenancy:migrate', [
                '--website_id' => $website->id,
                '--path' => 'database/migrations/tenant/custom',
                    ]
            );
        }

        // associate the website with a hostname
        $hostname = new Hostname;
        $hostname->fqdn = strtolower($data['name']) . "." . env('APP_DOMAIN');

        if (!Hostname::where('fqdn', $hostname->fqdn)->first()) {
            $hostname = app(HostnameRepository::class)->create($hostname);
            app(HostnameRepository::class)->attach($hostname, $website);
            //  current connection: tenant
            // puts owner in tenant database
            $admin = new \App\Models\Tenant\User([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'is_owner' => true
            ]);

            $admin->save();

            return new Tenancy($website, $hostname);
        } else {
            return FALSE;
        }
    }

}
