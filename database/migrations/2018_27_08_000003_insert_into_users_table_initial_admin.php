<?php

use Illuminate\Database\Migrations\Migration;

class InsertIntoUsersTableInitialAdmin extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
            'name' => 'hyn admin',
            'email' => 'admin@tendegrees.sa',
            'password' => Hash::make('test1234'),
            'created_at' => new \DateTime(),
            'updated_at' => new \DateTime(),
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->where([['email', '=', 'admin@tendegrees.sa']])->delete();
    }
}
