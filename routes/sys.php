<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group([
    'domain' => env('APP_DOMAIN'),
        ], function () {
    Route::group(['namespace' => 'System'], function () {
        Auth::routes();

        Route::get('/', function () {
            return view('system.welcome');
        })->name('/');

//        Route::group([
//            'middleware' => ['auth'],
//                ], function () {
        Route::get('/home', 'HomeController@index')->name('home');
//        });
    });
});
