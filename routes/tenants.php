<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//dd('2xxx');

//dd(env('APP_DOMAIN') . '11');


Route::group([
    'domain' => '{companyIdentifier}.' . env('APP_DOMAIN'), 'namespace' => 'App\Http\Controllers'
        ], function () {

//    Auth::routes();

    Route::get('/', function () {
        return view('welcome');
    })->name('/');



//            Route::get('/home', 'SassHomeController@index')->name('home');


//    Route::group([
//        'middleware' => ['auth'],
//            ], function () {
    Route::get('/home', 'HomeController@index')->name('home');
//    });
});
